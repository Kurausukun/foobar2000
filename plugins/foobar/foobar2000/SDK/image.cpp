#include "foobar2000.h"
#include "image.h"

namespace fb2k {
    bool imageSize_t::equals(imageSize_t const & v1, imageSize_t const & v2) {
        return v1.width == v2.width && v1.height == v2.height;
    }
    bool imageSize_t::greater(const fb2k::imageSize_t &v1, const fb2k::imageSize_t &v2) {
        // v1 > v2
        if (v1.width > v2.width) return true;
        if (v1.width < v2.width) return false;
        return v1.height > v2.height;
    }
    bool imageSize_t::isValid() const {
        return width > 0 && height > 0;
    }

    imageSize_t imageSize_t::fitIn( double longerEdge ) const {
        if (!isValid() || longerEdge <= 0) return empty();
        
        if (height <= longerEdge && width <= longerEdge) return *this;
        
        imageSize_t rv = {};
        if (width > height) {
            rv.width = longerEdge;
            rv.height = longerEdge * height / width;
        } else {
            rv.height = longerEdge;
            rv.width = longerEdge * width / height;
        }
        return rv;
    }
    imageSize_t imageSize_t::fitIn( imageSize_t size ) const {
        if (!isValid() || !size.isValid()) return empty();
        
        if (width <= size.width && height <= size.height) return *this;
        
        imageSize_t rv = {};
        double ratio = size.width / size.height;
        double ourRatio = width / height;
        if (ratio < ourRatio) {
            // fit width
            rv.width = size.width;
            rv.height = height * (size.width / width);
        } else {
            // fit height
            rv.height = size.height;
            rv.width = width * (size.height / height);
        }
        return rv;
    }
    
    void imageSize_t::sanitize() {
        if (width < 0) width = 0;
        if (height < 0) height = 0;
        width = pfc::rint32(width);
        height = pfc::rint32(height);
    }
    image::ptr image::resizeToFit( imageSize_t fitInSize ) {
        fitInSize.sanitize();
        imageSize_t ourSize = size();
        imageSize_t resizeTo = ourSize.fitIn(fitInSize);
        if (resizeTo == ourSize || !resizeTo.isValid()) return this;
        return resize( resizeTo );
    }



	static imageSize_t imageSizeSafe(imageRef img) {
		if (img.is_valid()) return img->size();
		else return imageSize_t::empty();
	}
	imageRef imageCreator::loadImageNamed2( const char * imgName, arg_t & arg ) {
		auto ret = loadImageNamed( imgName, arg.inWantSize );
		arg.outRealSize = imageSizeSafe(ret);
		return ret;
	}
	imageRef imageCreator::loadImageData2( const void * data, size_t size, stringRef sourceURL, arg_t & arg ) {
		auto ret = loadImageData(data, size, sourceURL);
		arg.outRealSize = imageSizeSafe(ret);
		return ret;
	}
	imageRef imageCreator::loadImageData2(memBlockRef block, stringRef sourceURL, arg_t & arg) {
		auto ret = loadImageData(block, sourceURL);
		arg.outRealSize = imageSizeSafe(ret);
		return ret;
	}
	imageRef imageCreator::loadImageFromFile2(const char * filePath, abort_callback & aborter, arg_t & arg) {
		auto ret = loadImageFromFile(filePath, aborter);
		arg.outRealSize = imageSizeSafe(ret);
		return ret;
	}

}

pfc::string_base & operator<<(pfc::string_base & p_fmt,const fb2k::imageSize_t & imgSize) {
	auto iw = pfc::rint32(imgSize.width);
	auto ih = pfc::rint32(imgSize.height);
	if (iw == imgSize.width && ih == imgSize.height) {
        return p_fmt << "(" << iw << "x" << ih << ")";
	} else {
        return p_fmt << "(" << imgSize.width << "x" << imgSize.height << ")";
	}
}

